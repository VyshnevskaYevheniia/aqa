package Week3;

import org.hamcrest.core.IsEqual;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import static org.testng.Assert.assertEquals;

import static io.restassured.RestAssured.given;

public class NewUser {
    @BeforeTest
    public void beforeTest()
    { System.out.print("1 - beforeTest");
    }
        @Test
        void addNewUser() {
            String token = "26d768b645e81245445561c6e9d8625fb2f1f798e1c3f408c672e7a344070c4b";
            String email = "user5@gmailp.com";
            String userData = "{\r\n"
                    + "        \"name\": \"Ronald Reigan_test2\",\r\n"
                    + "        \"email\": \""+email+"\",\r\n"
                    + "        \"gender\": \"male\",\r\n"
                    + "        \"status\": \"active\"\r\n"
                    + "    }";
            RestAssured.baseURI = "https://gorest.co.in";
            RestAssured.basePath = "public/v2/users";
           
            given()
                    .auth().oauth2(token)
                    .body(userData).contentType(ContentType.JSON)
                    .log()
                    .all()
                    .post()
                    .then()
                    .log()
                    .all()
                    .assertThat()
                    .statusCode(201)
                    .body("email", IsEqual.equalTo(email));


        }


}

